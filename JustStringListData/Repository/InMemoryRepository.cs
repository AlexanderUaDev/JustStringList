﻿using JustStringListData.Interfaces;
using JustStringListData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace JustStringListData.Repository {
    public class InMemoryRepository : IStringsItemRepository, IDisposable {
        private DataTable _storeTable;
        public InMemoryRepository() {
            _storeTable = new DataTable("StringItems");

            DataColumn column = new DataColumn("Id");
            column.DataType = System.Type.GetType("System.Int32");
            column.AutoIncrement = true;
            column.AutoIncrementStep = 1;

            _storeTable.Columns.Add(column);
            _storeTable.Columns.Add("Value", System.Type.GetType("System.String"));
            _storeTable.Columns.Add("OrderInList", System.Type.GetType("System.Int32"));
        }

        public IEnumerable<Models.StringItem> GetStringItems() {
            return _storeTable.AsEnumerable().Select(x => new StringItem() { ID = int.Parse(x[0].ToString()), Value = x[1].ToString(), OrderInList = int.Parse(x[2].ToString()) }).ToList();
        }

        public int InsertString(string stringItem) {
            var maxOrder = _storeTable.AsEnumerable().Max(x => x[2]);
            Int32 neworder = maxOrder == null ? 0 : int.Parse(maxOrder.ToString());
            neworder++;
            _storeTable.Rows.Add(null, stringItem, neworder);
            return int.Parse(_storeTable.AsEnumerable().LastOrDefault()[0].ToString());
        }

        public void DeleteString(int stringItemID) {
            var tmpRowCol = _storeTable.Select("Id=" + stringItemID.ToString());
            if (tmpRowCol != null && tmpRowCol.Length > 0) {
                _storeTable.Rows.Remove((DataRow)tmpRowCol[0]);
            }
        }

        public void UpdateString(int Id, string stringValue) {
            var tmpRowCol = _storeTable.Select("Id=" + Id.ToString());
            if (tmpRowCol != null && tmpRowCol.Length > 0) {
                tmpRowCol[0][1] = stringValue;
            }
        }

        public void ChangeOrder(int stringItemId, int selectedItemIndex, int direction) {

            var items = _storeTable.AsEnumerable().OrderBy(x => x[2]).ToList();
            if (items != null && items.Count > 0) {
                var secItemIndex = direction == 1 ? selectedItemIndex - 1 : selectedItemIndex + 1;

                var tmpOrder = items[secItemIndex][2];
                items[secItemIndex][2] = items[selectedItemIndex][2];
                items[selectedItemIndex][2] = tmpOrder;
            }
        }

        public void Dispose() {
            if (_storeTable != null) {
                _storeTable.Dispose();
            }
        }
    }
}
