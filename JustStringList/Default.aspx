﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="JustStringList.Default" %>

<%@ Register TagPrefix="UC" TagName="BarControl" Src="UserControls/ItemBar.ascx" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="Style.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Simple String List</title>

</head>
<body>
    <form id="formList" runat="server">
        <div id="container">
            <div id="listContent">
                <table id="listOfStrings" runat="server">
                </table>
            </div>
            <div id="barContent" style="display:none">
                <UC:BarControl ID="itemBar" runat="server"  style="display:none"/>
            </div>
        </div>
            <asp:HiddenField runat="server" id="currentRow"/>
            <asp:HiddenField runat="server" id="itemEditMode" Value="0"/>
        <hr />
        <asp:TextBox ID="stringEditField" runat="server" Style="display: none"></asp:TextBox>
        <table>
            <tr>
                <td>
                    <input id="addBtn" type="button" value="Add" />
                </td>
            </tr>
        </table>

        <div></div>
    </form>    
</body>
<script src="Scripts/uiScript.js"></script>
</html>
