﻿using JustStringListData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustStringListData.Interfaces {
    public interface IStringsItemRepository :IDisposable{
        IEnumerable<StringItem> GetStringItems();
        int InsertString(String stringItem);
        void DeleteString(int stringItemID);
        void UpdateString(int Id, string stringValue);
        void ChangeOrder(int stringItemId, int order, int direction);
    }
}
