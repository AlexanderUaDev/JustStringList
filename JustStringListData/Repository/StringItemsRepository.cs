﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JustStringListData.Interfaces;
using JustStringListData.Models;
using System.Data.SQLite;
using System.Data;


namespace JustStringListData.Repository {
    public class StringItemsRepository : IStringsItemRepository, IDisposable {
        private SQLiteConnection _connection;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();

        public StringItemsRepository(string connection) {
            _connection = new SQLiteConnection(connection);
        }

        public IEnumerable<StringItem> GetStringItems() {
            _connection.Open();
            sql_cmd = _connection.CreateCommand();
            DB = new SQLiteDataAdapter("select * from StringItems", _connection);
            DS.Reset();
            DB.Fill(DS);
            var resultTable = DS.Tables[0];
            _connection.Close();

            return resultTable.AsEnumerable().Select(x => new StringItem() {
                ID = int.Parse(x[0].ToString()),
                Value = x[1].ToString(),
                OrderInList = int.Parse(x[2].ToString())
            }).ToList();
        }


        public void Dispose() {
            if (_connection != null) {
                _connection.Close();
                _connection.Dispose();
            }
        }

        private void ExecuteQuery(string txtQuery) {
            _connection.Open();
            sql_cmd = _connection.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            _connection.Close();
        }
        private object ExecuteScalarQuery(string txtQuery) {
            object result;
            _connection.Open();
            sql_cmd = _connection.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            result = sql_cmd.ExecuteScalar();
            _connection.Close();
            return result;
        }

        public int InsertString(string stringItem) {
            var orderRes = ExecuteScalarQuery("select max(OrderInList) from StringItems");
            Int32 order = !string.IsNullOrEmpty(orderRes.ToString()) ? int.Parse(orderRes.ToString()) : 0;
            order++;
            string txtSQLQuery = "insert into StringItems(Value,OrderInList) values ('" + stringItem + "'," + (order++).ToString() + ");SELECT last_insert_rowid();";

            Int32 lastId = int.Parse(ExecuteScalarQuery(txtSQLQuery).ToString());
            return lastId;
        }

        public void DeleteString(int stringItemID) {
            string txtSQLQuery = "delete from StringItems where Id=" + stringItemID.ToString();
            ExecuteQuery(txtSQLQuery);
        }

        public void UpdateString(int Id, string stringItem) {
            string txtSQLQuery = "update StringItems set Value='" + stringItem + "' where Id=" + Id.ToString();
            ExecuteQuery(txtSQLQuery);
        }

        public void ChangeOrder(int stringItemId, int selectedItemIndex, int direction) {

            var items = GetStringItems().OrderBy(x=>x.OrderInList).ToList();
            if(items!=null && items.Count()>0){
                var secItemIndex = direction == 1 ? selectedItemIndex - 1 : selectedItemIndex + 1;
                
            StringBuilder sb = new StringBuilder("update StringItems set OrderInList=" + items[selectedItemIndex].OrderInList + " where Id=" + items[secItemIndex].ID.ToString() + ";");
            sb.AppendLine("update StringItems set OrderInList=" + items[secItemIndex].OrderInList.ToString() + " where Id=" + items[selectedItemIndex].ID.ToString() + ";");

            ExecuteQuery(sb.ToString());
            }            
        }
    }
}
