﻿using JustStringListData.Interfaces;
using JustStringListData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using JustStringListData.Repository;

namespace JustStringList {
    public partial class Default : System.Web.UI.Page {
        private static IStringsItemRepository _contextRep {
            get {
                if (UseInMemoryRepository) {
                    return (IStringsItemRepository)HttpContext.Current.Application["Repository"];
                } else {
                    return new StringItemsRepository(DBConnnString);
                }
            }
        }

        private static string DBConnnString {
            get {
                return "data source=" + System.Web.Hosting.HostingEnvironment.MapPath(System.Configuration.ConfigurationManager.ConnectionStrings["SQLiteConnection"].ConnectionString);
            }
        }
        private static bool UseInMemoryRepository {
            get {
                return bool.Parse(System.Configuration.ConfigurationManager.AppSettings["UseInMemory"]);
            }
        }

        protected override void OnInit(EventArgs e) {
            base.OnInit(e);
            if (UseInMemoryRepository && HttpContext.Current.Application["Repository"] == null) {
                HttpContext.Current.Application["Repository"] = new InMemoryRepository();
            }
        }
        protected void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                try {
                    var items = _contextRep.GetStringItems().OrderBy(x => x.OrderInList);
                    foreach (var item in items) {
                        HtmlTableRow tr = new HtmlTableRow();
                        tr.Attributes.Add("data-rowId", item.ID.ToString());
                        var tmpCell = new HtmlTableCell();
                        tmpCell.InnerHtml = "<a>" + item.Value + "</a>";
                        tr.Cells.Add(tmpCell);

                        listOfStrings.Rows.Add(tr);
                    }
                } catch (Exception ex) {
                }
            }
        }

        [WebMethod]
        public static int AddString(string addValue) {
            int insertedId = -1;
            try {
                var rep = (InMemoryRepository)HttpContext.Current.Application["Repository"];

                insertedId = _contextRep.InsertString(addValue);
            } catch (Exception ex) {
                return -1;
            }
            return insertedId;
        }

        [WebMethod]
        public static bool RemoveString(int itemId) {
            try {
                _contextRep.DeleteString(itemId);
            } catch (Exception ex) {
                return false;
            }
            return true;
        }

        [WebMethod]
        public static bool SaveString(int itemId, string newValue) {
            try {
                _contextRep.UpdateString(itemId, newValue);
            } catch (Exception ex) {
                return false;
            }
            return true;
        }

        [WebMethod]
        public static bool ChangeOrder(int itemId, int orderValue, int direction) {
            try {
                _contextRep.ChangeOrder(itemId, orderValue, direction);
            } catch (Exception ex) {
                return false;
            }
            return true;
        }
    }
}