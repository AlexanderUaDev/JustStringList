﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustStringListData.Models {
    public class StringItem {
        public int ID { get; set; }
        public string Value { get; set; }
        public int OrderInList { get; set; }
    }
}
