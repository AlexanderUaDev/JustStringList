﻿$('#formList').on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});

$("#stringEditField").keypress(function (e) {
    if (e.keyCode == 13) {
        stringProcessing();
    }
});

function stringProcessing() {
    var editItem = $("#stringEditField");

    if (editItem.is(":visible")) {
        editItem.hide();
        $("#addBtn").val("Add");
        saveString(editItem.val());
        editItem.val('');
    }
    else {
        setUIEditMode();
    }

}

function hideBar() {
    $('#currentRow').val('');
    $('#listOfStrings').find("a").removeClass("selected");
    $('#barContent').hide();
}

function setUIEditMode() {
    var editItem = $("#stringEditField");
    editItem.show();
    editItem.focus();
    $("#addBtn").val("Save");
}
function saveString(itemText) {

    if ($('#itemEditMode').val() == 0) {
        if (itemText != '') {

            var postData = { "addValue": itemText };
            $.ajax({
                type: "POST",
                url: "Default.aspx/AddString",
                data: JSON.stringify(postData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (retData) {
                    if ($.isNumeric(retData.d)) {
                        $('#listOfStrings').append("<tr data-rowid='" + retData.d + "'><td><a>" + itemText + "</a></td></tr>");
                        $('#listOfStrings tr:last').on('click', null, function (e) {
                            showBar(this);
                        });
                    } else {
                        alert('Error saving of string item.');
                    }
                },
                error: function () {
                    alert('Error saving of string item.');
                }
            });

        }
    }
    else {
        var selIndex = $('#stringEditField').data('editedRowIndex');
        var itemId = $(listOfStrings.rows[selIndex]).data('rowid');
        if (selIndex != -1 && typeof itemId !== 'undefined') {
            var postData = { "itemId": itemId, "newValue": itemText };
            $.ajax({
                type: "POST",
                url: "Default.aspx/SaveString",
                data: JSON.stringify(postData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (retData) {
                    if (retData) {
                        $(listOfStrings.rows[selIndex]).find('a')[0].text = itemText == '' ? '(There is no text)' : itemText;
                        $('#itemEditMode').val(0);
                    } else {
                        alert('Error saving of string item.');
                    }
                },
                error: function () {
                    alert('Error saving of string item.');
                }
            });
        }
    }
}

function showBar(row) {
    if (typeof row === "undefined")
        return;
    hideBar();
    var rowItem = $(row);

    rowItem.find("a").addClass("selected");

    $('#currentRow').val(row.rowIndex);

    var rowItemPos = rowItem.position();
    var bItem = $('#barContent');
    bItem.css('top', rowItemPos.top + 'px');
    var newLeftPos = rowItem[0].clientWidth;
    bItem.css('left', newLeftPos + 'px');
    bItem.show();
    if (typeof event !== 'undefined') {
        event.stopPropagation();
    }
}

function addRowHandlers() {
    $("#listOfStrings tr").click(function () { showBar(this); });
}


function stringMove(direction) {

    var selIndex = Number($('#currentRow').val());

    var selRowId = $(listOfStrings.rows[selIndex]).data('rowid');
    var rowsCount = Number(listOfStrings.rows.length);

    if (direction == 1)//up
    {
        if (selIndex != 0) {
            if (getCurrentItemId() != -1) {
               
                var postData = { "itemId": getCurrentItemId(), "orderValue": selIndex, "direction": "1" };
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/ChangeOrder",
                    data: JSON.stringify(postData),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    // async: false,
                    success: function (msg) {
                        if (msg.d) {
                            $(listOfStrings.rows[selIndex - 1]).insertAfter($(listOfStrings.rows[selIndex]));
                            selIndex--;
                            $('#currentRow').val(selIndex);
                            showBar(listOfStrings.rows[selIndex]);
                        } else { alert('Error change order.'); }
                    },
                    error: function () {
                        alert('Error change order.');
                    }
                });
            }

        }
    }
    else {//0 - down
        if (selIndex != rowsCount - 1 && getCurrentItemId() != -1) {
            var postData = { "itemId": getCurrentItemId(), "orderValue": selIndex, "direction": "0" };
            $.ajax({
                type: "POST",
                url: "Default.aspx/ChangeOrder",
                data: JSON.stringify(postData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //async:false,
                success: function (msg) {
                    if (msg.d) {
                        $(listOfStrings.rows[selIndex + 1]).insertBefore($(listOfStrings.rows[selIndex]));
                        selIndex++;
                        $('#currentRow').val(selIndex);
                        showBar(listOfStrings.rows[selIndex]);
                    } else {
                        alert('Error change order.');
                    }
                },
                error: function () {
                    alert('Error change order.');
                }
            });
        }
    }

}

function editStringItem() {
    var selIndex = $('#currentRow').val();

    $('#stringEditField').val(listOfStrings.rows[selIndex].innerText);
    $('#stringEditField').data('editedRowIndex', selIndex)
    setUIEditMode();
    $('#itemEditMode').val(1);
}

function deleteStringItem() {
    var r = confirm("Are you sure?");
    if (r == true && getCurrentItemId() != -1) {
        var selIndex = $('#currentRow').val();
        var postData = { "itemId": getCurrentItemId() };
        $.ajax({
            type: "POST",
            url: "Default.aspx/RemoveString",
            data: JSON.stringify(postData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg.d) {
                    $(listOfStrings.rows[selIndex]).remove();
                } else { alert('Error delete item.'); }
            },
            error: function () {
                alert('Error delete item.');
            }
        });
    }

}
function getCurrentItemId() {
    var curRow = $('#currentRow').val();
    if (typeof curRow === 'undefined' || curRow == '') {
        return -1;
    }
    var selIndex = Number($('#currentRow').val());
    var selRowId = $(listOfStrings.rows[selIndex]).data('rowid');
    return selRowId;
}
$(document).click(function () {
    hideBar();
});

$(document).ready(function () {
    addRowHandlers();

    $("#addBtn").click(function () {
        stringProcessing();
    });

    $("#btnUp").click(function () {
        stringMove(1);
    });

    $("#btnDown").click(function () {
        stringMove(0);
    });

    $("#btnDots").click(function () {
        editStringItem();
        //hideBar();
    });

    $("#btnDelete").click(function () {
        deleteStringItem();
        //hideBar();
    });
});